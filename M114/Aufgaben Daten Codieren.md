# 1

| DEC | HEX | BIN 2^3 | BIN 2^2 | BIN 2^1 | BIN 2^0 |
| --- | --- | ------- | ------- | ------- | ------- |
| 0   | 0   | 0       | 0       | 0       | 0       |
| 1   | 1   | 0       | 0       | 0       | 1       |
| 2   | 2   | 0       | 0       | 1       | 0       |
| 3   | 3   | 0       | 0       | 1       | 1       |
| 4   | 4   | 0       | 1       | 0       | 0       |
| 5   | 5   | 0       | 1       | 0       | 1       |
| 6   | 6   | 0       | 1       | 1       | 0       |
| 7   | 7   | 0       | 1       | 1       | 1       |
| 8   | 8   | 1       | 0       | 0       | 0       |
| 9   | 9   | 1       | 0       | 0       | 1       |
| 10  | A   | 1       | 0       | 1       | 0       |
| 11  | B   | 1       | 0       | 1       | 1       |
| 12  | C   | 1       | 1       | 0       | 0       |
| 13  | D   | 1       | 1       | 0       | 1       |
| 14  | E   | 1       | 1       | 1       | 0       |
| 15  | F   | 1       | 1       | 1       | 1       |
# 2
911 in Binär
911 - 512 = 399
399 - 256 = 143
143 - 128 = 15
.
.
.
15 - 8 = 7
7 - 4 = 3
3 - 2 = 1
1 - 1 = 0

111000111

# 3 
2 + 4 + 16 + 32 + 128 = 182

# 4
1110 = 8420 = 14 =E
0010 = 0020 = 2 = 2
1010 = 8020 = 10 = A
0101 = 0401 = 5 = 5
E2C5

# 5
1101 1001 +
0111 0101 =
_________
(1) 0100 1110
Data Overflow!

# 6
a. 120.A8.4C.D5 Das wird wahrscheinlich eine IP-Adresse sein
b. DE-83-85-D5-E4-FE Das ist wahrscheinlich die MAC-Adresse

# 8
7 Bit werden benötigt (2^7 = 128)

# 11
a) 1 Byte = 8 Bit = 256 mögliche Zeichen = Zahlen von 0 bis 255
b) Vorzeichenbehaftet wären dann die Zahlen -128 bis 127 möglich
c) 0 fängt immer noch bei 0000 an und alle negativen Zahlen fangen mit 1 an. 0111 ist also 127 also fängt -128 bei 1000 an. 
83 = 1