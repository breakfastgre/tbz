# Creation
```Powershell
docker pull busybox
# Netzwerk createn
docker network create --subnet=172.18.0.0/16 tbz
# Container createn und mit default bridge Netzwerk verbinden
docker run --name busybox1 -d busybox:latest sleep 1000
docker run --name busybox2 -d busybox:latest sleep 1000
# Container createn und mit tbz Netzwerk verbinden
docker run --name busybox3 -d --network tbz busybox:latest sleep 1000 
docker run --name busybox4 -d --network tbz busybox:latest sleep 1000
``` 
![alt text](image-34673.png)

# Aufgabe 1
Busybox1:
- IP: 172.17.0.2
- Gateway: 172.17.0.1

Busybox2:
- IP: 172.17.0.3
- Gateway: 172.17.0.1

Busybox3:
- IP: 172.18.0.2
- Gateway: 172.18.0.1

Busybox4:
- IP: 172.18.0.3
- Gateway: 172.18.0.1

# Aufgabe 2
Busybox2 hat denselben Gateway
![alt text](image-92849.png)

# Aufgabe 3
Busybox4 hat denselben Gateway
![alt text](image-021920.png)
Ping an IP Adresse von Busybox1 & 4 funktionieren ebenfalls

# Erklärung KN02
Die beiden befanden sich in dem default Gateway, konnten kommunizieren über die IP Adresse, nicht jedoch über den Namen.