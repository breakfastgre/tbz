﻿# Aufgabe 1
docker run -d -p 80:80 docker/getting-started

# 1. Version Docker Desktop erfragen
#wsl --status

# 2. ubuntu & nginx Docker-Image auf Hub suchen
#docker search ubuntu
#docker search nginx

# 3. "docker run" startet Container -> "-d" Führt Container im Hintergrund aus -> "-p 80:80" mapped Port 80 des Host auf Port 80 des Containers -> "docker/getting-started" Dieses Image startet den Container
# 4. Beweis, dass docker pull, create & start das gleiche wie docker run ist
#docker pull nginx
#docker create -p 8081:80 --name Aufgabe2 nginx
#docker start Aufgabe2

# 5. ubuntu-Image im Hintergrund ausführen
#docker run -d -p 8082:80 ubuntu
# Es wird ein Image generiert, aber der Container läuft nicht. 
#docker run -it ubuntu #-it startet Prozess, welcher auf Eingabe damit der container nicht auutomatisch wieder endet
# Nun läuft der Container

# 6. Interaktive Shell 
#docker exec -it Aufgabe2 /bin/bash
#service nginx status
#exit

# 7. Status von Container überprüfen
#docker ps -a

# 8. nginx Container stoppen
#docker stop Aufgabe2

# 9. Alle Container entfernen
#docker rm $(docker ps -aq)

# 10. Entfernen der beiden Images
#docker rmi nginx
#docker rmi ubuntu

# Aufgabe D
#docker pull nginx
#docker tag nginx:latest milchschoggi/m347:nginx # damit fügen wir dem image nginx mit Tag 'latest' einen neuen Tag hinzu
#docker push milchschoggi/m347:nginx # Nun haben wir das Image auf den Hub gepusht
#docker pull mariadb
#docker tag mariadb:latest milchschoggi/m347:mariadb
#docker push milchschoggi/m347:mariadb