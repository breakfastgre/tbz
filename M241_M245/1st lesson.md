Design Thinking beginnt mit Problem/Challenge.
Arten von Innovation
- Inkrementell: beruht auf bestehende Innovationen, macht Markt nicht kaputt
- radikal: beruht auf neuer Innovation, macht Markt nicht kaputt
- disruptiv: beruht auf neuer Innovation, bestehende Marktführer gehen evt. Konkurs, anfangs schlechter
Killer-App: App, welche eine Plattform kaufwert macht. 

Innovation nur Innovation, wenn sie erfolgreich in den Markt eingeführt wird.

![[Drawing 2025-01-20 11.11.27.excalidraw]]

# Challenge 1
### Problemraum
**Problem verstehen**
- Zu schlecht sichtbar
- Getrennt wird: Papier, Alu & PET, Restmüll
- Organisatorisches Problem
**Bedürfnisse sammeln**
- Müll getrennt haben
- Sichtbare Mülltrennung
**Challenge definieren**
- Visuell klar machen
### Lösungsraum
**Ideen entwickeln**
- 