# A
![alt text](../../ZZZ_Images/image-83858.png)

``` Javascript
// minecraft
db.createUser(
    {
        user : "user1",
        pwd : "kauderwelsch",
        roles : [
            {role : "read", db: "minecraft"}
        ]
    }
); 

//admin
db.createUser(
    {
        user : "user2",
        pwd : "ksdjbfjsabghvdqwzu8438934",
        roles : [
            {role : "readWrite", db: "minecraft"}
        ]
    }
); 
```

## User1
![alt text](../../ZZZ_Images/Pasted%20image%2020240627112115.png)
![alt text](../../ZZZ_Images/Pasted%20image%2020240627112150.png)
![alt text](../../ZZZ_Images/Pasted%20image%2020240627112214.png)


## User2
![alt text](../../ZZZ_Images/Pasted%20image%2020240627112311.png)
![alt text](../../ZZZ_Images/Pasted%20image%2020240627112332.png)
![alt text](../../ZZZ_Images/Pasted%20image%2020240627112357.png)

# B
Variante 1
![alt text](../../ZZZ_Images/Pasted%20image%2020240627113103.png)
``` Powershell
db.biomes.drop()
```
![alt text](../../ZZZ_Images/image-998789.png)
![alt text](../../ZZZ_Images/image-2948.png)

Variante 2
``` Powershell
db.grantRolesToUser("admin", [{ role: "root", db: "admin" }])
sudo mongodump --db minecraft --uri="mongodb://admin:bobr.91Kurwa@50.16.237.248:27017/?authSource=admin&readPreference=primary&ssl=false" --out /home/ubuntu
sudo mongorestore --username admin --password bobr.91Kurwa --authenticationDatabase admin --db minecraft /home/ubuntu/minecraft
```
![alt text](../../ZZZ_Images/image-899299.png)
![alt text](../../ZZZ_Images/image-19297.png)

# C
Replication: Alle Daten werden vollständig auf weitere Instanzen kopiert. 
Partitions: Daten sind auf mehreren Instanzen aufgeteilt; zum Beispiel sind Kreditkarteninformationen auf einer Partition und die Person auf einer anderen.

![alt text](../../ZZZ_Images/partition-with-replication.jpg)
![Quelle](https://www.baeldung.com/cs/database-sharding-vs-partitioning)