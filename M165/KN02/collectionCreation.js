const { MongoClient } = require('mongodb');

async function main() {
    const uri = "mongodb://admin:bobr.91Kurwa@50.16.237.248:27017/?authSource=admin&readPreference=primary&ssl=false";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    try {
        // Verbinde zum MongoDB-Server
        await client.connect();

        /*// Führe eine einfache Operation aus, z.B. eine Datenbankliste abrufen
        const databasesList = await client.db().admin().listDatabases();
        console.log("Datenbanken:");
        databasesList.databases.forEach(db => console.log(` - ${db.name}`)); */

        // Datenbank erstellen
        const database = client.db('minecraft');
        await database.createCollection('biomes');
        await database.createCollection('structures');
        await database.createCollection('mobs');
        await database.createCollection('items');

    } catch (e) {
        console.error(e);
    } finally {
        // Verbindung schliessen
        await client.close();
    }
}

main().catch(console.error);
