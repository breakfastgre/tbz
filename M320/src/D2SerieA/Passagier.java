package D2SerieA;

public class Passagier {
    String name;

    public Passagier(String name) {
        this.name = name;
    }

    void nameAusgeben() {
        System.out.println(name);
    }
}
