package D2SerieA;

import java.util.ArrayList;
import java.util.List;

public class Flug {
    String Flugnummer;
    List<Passagier> passagiere = new ArrayList<Passagier>();

    public Flug(String flugnummer) {
        Flugnummer = flugnummer;
    }

    public String getFlugNummer() {
        return this.Flugnummer;
    }

    void passagierListeAusgeben(){
        for(Passagier p: passagiere) {
            p.nameAusgeben();
        }
    }
}
