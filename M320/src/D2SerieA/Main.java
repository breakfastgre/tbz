package D2SerieA;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Defining stuff
        Flug newFlug = new Flug("LH4948");
        Flug newFlug2 = new Flug("LH4919");
        List<Flug> fluege = new ArrayList<Flug>();
        String User;
        String Flug;
        Scanner myObj = new Scanner(System.in);
        int counter = 0;

        // Erstellte Fluge in Listen eintragen
        fluege.add(newFlug);
        fluege.add(newFlug2);

        // User registrieren
        System.out.println("Guten Tag. Wie heissen Sie? ");
        User = myObj.nextLine();
        Passagier newPassagier = new Passagier(User);

        // In Flug eintragen
        System.out.println("In welchen Flug müssen wir Sie eintragen? ");
        Flug = myObj.nextLine();
        do  {
            if(Objects.equals(fluege.get(counter).getFlugNummer(), Flug)) {
                fluege.get(counter).passagiere.add(newPassagier);
                return;
            } else if(!Objects.equals(fluege.get(counter).Flugnummer, Flug) && counter == fluege.size() -1) {
                System.out.println("Kein Flug gefunden");
                counter++;
            } else {
                counter++;
            }
        } while(counter <= (fluege.size() -1));
    }
}
