package V3SerieA;

import java.util.ArrayList;

public class NYSE implements StockExchange{
    ArrayList<Aktie> NYSEShares = new ArrayList<>();

    public NYSE() {
        Aktie microsoft = new Aktie("Microsoft",100);
        addNYSE(microsoft);
        Aktie apple = new Aktie("Apple",70);
        addNYSE(apple);
        Aktie UBS = new Aktie("UBS", 88888);
        addNYSE(UBS);
        Aktie BSU = new Aktie("BSU", 2);
        addNYSE(BSU);
    }

    @Override
    public Integer getPrice(String aktName) {
        for (Aktie aktie:NYSEShares) {
            if(aktName == aktie.name) {
                return aktie.price;
            }
        }
        return null;
    }

    public void addNYSE(Aktie aktieNYSE) {
        NYSEShares.add(aktieNYSE);
    }
}
