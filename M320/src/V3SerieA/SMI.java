package V3SerieA;

import java.util.ArrayList;

public class SMI implements StockExchange {
    ArrayList<Aktie> SMIShares = new ArrayList<>();

    public void addSMI(Aktie aktieSMI) {
        SMIShares.add(aktieSMI);
    }

    public SMI() {
        Aktie microsoft = new Aktie("Microsoft",120);
        addSMI(microsoft);
        Aktie apple = new Aktie("Apple",80);
        addSMI(apple);
        Aktie UBS = new Aktie("UBS", 9999999);
        addSMI(UBS);
        Aktie BSU = new Aktie("BSU", 3);
        addSMI(BSU);
    }

    @Override
    public Integer getPrice(String aktName) {
        for (Aktie aktie:SMIShares) {
            if(aktName == aktie.name) {
                return aktie.price;
            }
        }
        return null;
    }
}
