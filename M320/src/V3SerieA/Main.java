package V3SerieA;

public class Main {
    public static void main(String[] args) {
        Portfolio portfolioGreg = new Portfolio();

        StockExchange nex = new NYSE();
        StockExchange stex = new SMI();

        System.out.println("Price of Microsoft share in ZH: " + stex.getPrice("Microsoft"));
        System.out.println("Price of Apple share in ZH: " + stex.getPrice("Apple"));
        System.out.println("Price of UBS share in ZH: " + stex.getPrice("UBS"));
        System.out.println("Price of BSU share in ZH: " + stex.getPrice("BSU"));

        System.out.println("\nPrice of Microsoft share in NY: " + nex.getPrice("Microsoft"));
        System.out.println("Price of Apple share in NY: " + nex.getPrice("Apple"));
        System.out.println("Price of UBS share in NY: " + nex.getPrice("UBS"));
        System.out.println("Price of BSU share in NY: " + nex.getPrice("BSU"));

        System.out.println("\nThis is going to return null: " + nex.getPrice("Beni"));



    }
}
