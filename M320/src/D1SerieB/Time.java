package D1SerieB;

public class Time {
    private int second = 0;
    private int minute = 0;
    private int hour = 0;

    public Time() {
    }

    public Time(int second, int minute, int hour) {
        try {
            validateTime(second, minute, hour);
        } catch (Exception e) {
            System.out.println("Your date is invalid!");
            // This exits the function
            return;
        }
        this.second = second;
        this.minute = minute;
        this.hour = hour;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String toString() {
        String currentTime = (String.format("%02d", this.hour) + ":" + String.format("%02d", this.minute) + ":" + String.format("%02d", this.second));
        return currentTime;
    }

    public void validateTime(int second, int minute, int hour) throws Exception {
        if (second > 60 || second < 0 || minute > 60 || minute < 0 || hour > 24 || hour < 0) {
            throw new Exception("Your date is invalid!");
        }
    }
}
