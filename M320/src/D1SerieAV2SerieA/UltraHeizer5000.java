package D1SerieAV2SerieA;

public class UltraHeizer5000 extends SuperHeizer3000 {
    private String faehigkeit;
    public UltraHeizer5000(int min, int max, String name, String faehigkeit) {
        super(min, max, name);
        this.faehigkeit = faehigkeit;
    }

    public void printOut() {
        System.out.println("Das ist die printOut-Funktion der Sub-Klasse " + this.getName());
    }
}
