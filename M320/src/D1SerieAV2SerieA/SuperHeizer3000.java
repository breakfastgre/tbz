package D1SerieAV2SerieA;

public class SuperHeizer3000 extends Heizung {
    private String name;
    public SuperHeizer3000(int min, int max, String name) {
        super(min, max);
        this.name = name;
    }

    @Override
    public void printOut() {
        System.out.println("Das ist die printOut-Funktion der Sub-Klasse " + this.name);
    }

    public String getName() {
        return this.name;
    }
}
