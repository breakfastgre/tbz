package V1SerieA;

import D1SerieAV2SerieA.Heizung;
import D1SerieAV2SerieA.SuperHeizer3000;
import D1SerieAV2SerieA.UltraHeizer5000;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Heizung firstHeizung = new Heizung(3,80);
        Heizung secondHeizung = new Heizung(5,60);

        List<Heizung> heizungStack = new ArrayList<Heizung>();
        heizungStack.add(firstHeizung);
        heizungStack.add(secondHeizung);

        Stack<Heizung> newStack = new Stack(heizungStack);

        while (newStack.hasNext()) {
            //newStack.remove();
            System.out.println(newStack.next());
            //System.out.println(newStack.getTrolololList().size());
        }

        SuperHeizer3000 bessererHeizer = new SuperHeizer3000(5,10,"bessererHeizer");
        bessererHeizer.printOut();

        UltraHeizer5000 besterHeizer = new UltraHeizer5000(4,90,"besterHeizer", "Geistereinsauger");
        besterHeizer.printOut();
    }
}
