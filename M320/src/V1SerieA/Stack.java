package V1SerieA;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// trololol is a Platzhalter for the Object-Type
public class Stack<trololol> implements Iterator {
    List<trololol> trolololList = new ArrayList<trololol>();

    int currentTrololol = -2;

    public Stack(List<trololol> trolololList) {
        this.trolololList = trolololList;
    }

    public List<trololol> getTrolololList() {
        return trolololList;
    }

    public void setTrolololList(List<trololol> trolololList) {
        this.trolololList = trolololList;
    }

    public void Push(trololol e) {
        trolololList.add(e);
    }

    // Diese Funktion löscht immer den obersten Gegenstand des Stapels
    public trololol Pop() {
        trololol e = trolololList.get(trolololList.size());
        trolololList.remove(trolololList.size() -1);
        return e;
    }

    @Override
    public boolean hasNext() {
        if (currentTrololol == -2) {
            currentTrololol = trolololList.size() -1;
        }

        if (currentTrololol < 0) {
            currentTrololol = -2;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Object next() {
        currentTrololol--;
        return trolololList.get(currentTrololol + 1);
    }

    // Diese Funktion löscht das aktuell ausgewählte Objekt
    @Override
    public void remove() {
        next();
        trolololList.remove(currentTrololol + 1);
    }

    /*
    * Wir zählen im Schritt 'next' runter, damit wir das nächste Mal, wenn wir 'hasnext' aufrufen, überprüfen
    * können, ob es einen nächsten gibt.
    * currentTrololol: fängt bei -2 an, wird direkt auf die aktuelle Stapelgrösse gelegt und erst beim Schritt
    * 'next' runtergezählt.
    * */
}
