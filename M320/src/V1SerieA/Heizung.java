package V1SerieA;

public class Heizung {
    // Temperatur ist standardgemäss 15 Grad
    private int temperature = 15;
    private int min = 5;
    private int max = 60;
    private int increment = 5;

    public Heizung(/*int temperature, /*int increment,*/ int min, int max) {
        //this.temperature = temperature;
        //this.increment = increment;
        this.min = min;
        this.max = max;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    public void waermer(int i) {
        if (i > getMax()) {
            setTemperature(getMax());
            System.out.println("Die Temperatur darf nicht das maximum überschreiten! Temperatur wurde auf " +
                    getTemperature() + " gesetzt.");
        } else {
            setTemperature(i);
            System.out.println(getTemperature());
        }
    }

    public void printOut() {
        System.out.println("Das ist die printOut-Funktion der Super-Klasse Heizung!");
    }
}
