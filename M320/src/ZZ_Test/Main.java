package ZZ_Test;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        // Erstelle ein neues JFrame
        JFrame frame = new JFrame("Mein Fenster");

        // Setze die Größe des Fensters
        frame.setSize(400, 300);

        // Setze das Schließen-Verhalten
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Mache das Fenster sichtbar
        frame.setVisible(true);
    }
}