### **Mögliche Inhalte für die Reflektion:**
- Welche Vorbehalte haben Sie gegenüber dem heutigen Thema und Inhalt? Und wieso?
- Wie decken sich Ihre Erfahrungen in Ihrem Unternehmen mit den heutigen Inhalten?
- Was könnte man in Ihrem Unternehmen verbessern betreffendem Ihrem heutigen Thema?
- Ihr Team wird sich für eine Umsetzungsweise oder Herangehensweise entscheiden. Nicht unbedingt sind Sie damit einverstanden. Sie dürfen hier gerne auch alternative Möglichkeiten aufzeigen.

### **Bewertung**
- Regelmässigkeit und Kontinuität der Einträge. (Pro Modultag sollte mindestens ein vollständiger Eintrag vorhanden sein)
- Sinnhaftigkeit der Einträge. Logische Überlegen und Reflektionen.
- Gefühle gegenüber DevOps und deren Analyse, resp. woher kommen die und wie ist damit umzugehen
- Es ist der Lehrperson bewusst, dass eine solche tiefe Reflektion nicht unbedingt an jedem Modultag geschehen kann, da man evtl. an 2-3 Tagen am gleichen Thema arbeitet.
- Das Gesamtbild muss stimmen und es muss sichtbar werden, dass man sich tatsächlich persönlich und kritisch mit den Inhalten, sich selbst und der Firmenumgebung auseinandergesetzt hat.

### 26.02.2025
Das Thema interessiert mich sehr. Es erinnert micht etwas an Therapie aber für ein Geschäft. Die CALMS-Methode und three ways überschneiden sich oft, weshalb ich bei three ways Doku nicht immer 
alles nochmals aufschreiben wollte.
In meinem Geschäft ist DevOps zwar teilweise weit, aber nicht gleichmässig aufgebaut. 
- Culture: In einem Pod sind genau Developers und ein PO. Grundsätzlich habe ich noch nie gehört, dass der Betrieb die Schuld auf Devs zuwies. 
- Automation: CI/CD ist auf Dev vollständig implementiert. Bei TE1 und TE2 wird es von externen übernommen oder manuell gemacht. Dabei sind wir momentan daran,  die Tests weiter zu automatisieren. 
- Lean: Es gibt neue Tools die jedoch noch nicht gängig und implementiert sind. 
- Measurement: Kann ich generell nicht beurteilen, da ich nicht in der Teppich-Etage arbeite. 
- Sharing: Zwischen Devs gibt es bereits einige gute Möglichkeiten um sich gegenseitig auszutauschen. Zwischen Devs und Ops gibt es aber sicher noch Luft nach oben.

Bezüglich Fragen fände ich es besser, wenn unsere Aufgabe wär, der Firma Vorschläge zu geben. Bisher war es wirklich einfach dass die Situation war: Firma hat nicht CALMS implementiert, durch Schmidt haben sie CALMS implementiert. Dann mussten wir den Unterschied als Antwort geben. Der einzige Vorteil: man sieht direkt Beispiele für die einzelnen Aspekte. 
### 19.02.2025
Insgesamt kann ich die wichtigsten Punkte verstehen und nachvollziehen.
- Git als **Versionierungstool** ist am besten geeignet, da wir damit am meisten Erfahrung haben und zu wenig Probleme
- Das **Kanban-Board** mit Git ist eine gute Idee. Wir arbeiten in der Arbeit bereits mit Issues, jedoch nicht mit Kanban. Das wäre eine gute Idee, da damit auch wir Entwickler eine bessere Übersicht über die relevanten Issues haben. 
- **Programmiersprache**: Python habe ich bisher sehr wenig Erfahrung. Ich überlasse die API deshalb am besten meinen Mitgliedern, da MongoDB aufzusetzen vermutlich eher schnell gehen wird und ich genügend Zeit haben werde, mich etwas schlau zu machen. Mit Python werde ich bestimmt auch in späteren Issues zu tun haben, bei diesem Issue lasse ich es lieber, da ich den Fortschritt nicht bremsen möchte, der Fokus auf Dev Ops liegt und ich immer noch später mich schlau machen kann.
- **Datenbank**: Wir haben bereits Erfahrungen mit MongoDB als Cloud Datenbank. Jedoch habe ich nicht mehr in Erinnerung, wie die Verbindung von Flügen und Flughäfen dargestellt werden soll. Ein Grund mehr, um mich diesem Issue zu widmen!
