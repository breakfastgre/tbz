package SQLconnect;

import org.camunda.bpm.client.ExternalTaskClient;

import java.awt.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class DB_Cammunda_API {
    private final static Logger LOGGER = Logger.getLogger(DB_Cammunda_API.class.getName());

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("reminder-db")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {

                    String fetchWrite = externalTask.getVariable("fetchWrite");
                    String remDate = externalTask.getVariable("remDate");
                    String remTime = externalTask.getVariable("remTime");
                    String locRemText = externalTask.getVariable("locRemText");

                    String sDateTime = remDate + " " + remTime + ":00";
                    DBConnector myConnector = new DBConnector();

                    System.out.println("hiiii" + fetchWrite);

                    Map<String, Object> variablesMap = new HashMap<>();
                    if(fetchWrite.equals("write")){
                        //TODO: uncomment the following line for making Gregory's thing work
                        myConnector.createReminderSQL(sDateTime,locRemText);
                        System.out.println("I'm pretending to write a reminder right now");
                    }
                    if(fetchWrite.equals("fetch")){
                        //TODO: uncomment the following lines for making Gregory's thing work
                        locRemText = myConnector.checkSQL();
                        variablesMap.put("locRemText", locRemText);
                        //locRemText = "Finish Project";
                        //variablesMap.put("locRemText", locRemText);
                    }
                    externalTaskService.complete(externalTask, variablesMap);
                })
                .open();
    }
}