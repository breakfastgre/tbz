package SQLconnect;

import org.camunda.bpm.client.ExternalTaskClient;

import java.util.logging.Logger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DB_Snooze_API {
    private final static Logger LOGGER = Logger.getLogger(DB_Cammunda_API.class.getName());

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("snooze-db")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {

                    String locRemText = externalTask.getVariable("locRemText");
                    String snooze1h = externalTask.getVariable("snooze1h");
                    String sDateTime;
                    LocalDateTime modifiedDateTime;

                    LocalDateTime currentDateTime = LocalDateTime.now();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


                    if(snooze1h.equals("yes")){
                        modifiedDateTime = currentDateTime.plusHours(1);

                    } else {
                        modifiedDateTime = currentDateTime.plusDays(1);
                    }
                    sDateTime = modifiedDateTime.format(formatter);

                    DBConnector myConnector = new DBConnector();
                    //TODO: uncomment the following line for making Gregory's thing work
                    myConnector.createReminderSQL(sDateTime,locRemText);
                    System.out.println("I'm pretending to write a reminder right now");
                    System.out.println(locRemText);


                    externalTaskService.complete(externalTask);
                })
                .open();
    }
}