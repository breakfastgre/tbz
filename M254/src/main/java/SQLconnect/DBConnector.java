package SQLconnect;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DBConnector {


    public void createReminderSQL(String insDat, String insNot) {
        final String url = "jdbc:mysql://localhost:3306/terminbucher";
        final String username = "bermintucher";
        final String password = "bermintucher";

        // If connection works
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            String sqlStatementInsert = "INSERT INTO terminbucher (Datum, Notiz) VALUES (?, ?);";

            // Insert stuff into Database
            try (PreparedStatement pstmt = connection.prepareStatement(sqlStatementInsert)) {
                pstmt.setString(1, insDat);
                pstmt.setString(2, insNot);

                int rowsAffected = pstmt.executeUpdate();
                System.out.println(rowsAffected + " row(s) inserted.");
            } catch (SQLException e) {
                System.out.println("Error executing insert operation");
                e.printStackTrace();
            }
        } catch (SQLException e) {
            System.out.println("Error executing insert operation");
            e.printStackTrace();
        }
    }

    public String checkSQL() {
        final String url = "jdbc:mysql://localhost:3306/terminbucher";
        final String username = "bermintucher";
        final String password = "bermintucher";

        // If connection works
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            String sqlStatementCheck = "SELECT * FROM terminbucher WHERE Datum = ";

            // Get current time and check, whether reminder needs to be sent
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime now = LocalDateTime.now();
            String currentTime = dtf.format(now);

            try (Statement stmt = connection.createStatement();
                 ResultSet rs = stmt.executeQuery(sqlStatementCheck + "'" + currentTime + "';")) {
                boolean found = false;

                while (rs.next()) {
                    found = true;
                    String fetchDatum = rs.getString("Datum");
                    String fetchNotiz = rs.getString("Notiz");

                    return fetchNotiz;
                }

                if (!found) {
                    return "No reminder found";
                }
            } catch (SQLException e) {
                return "Error executing SELECT operation";
            }
            // If connection fails
        } catch (SQLException e) {
            return "Cannot connect the database!";
        }

        return "Something went wrong";
    }
}
