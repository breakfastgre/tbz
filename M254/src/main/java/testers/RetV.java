package testers;

import org.camunda.bpm.client.ExternalTaskClient;

import java.awt.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class RetV {
    private final static Logger LOGGER = Logger.getLogger(RetV.class.getName());

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("test-thing1")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {

                    Map<String, Object> variablesMap = new HashMap<>();
                    String myVariable = "Halleluja?";
                    System.out.println("made it into api 1");

                    LOGGER.info("Variable set: " + myVariable);

                    variablesMap.put("myVariable", myVariable);

                    externalTaskService.complete(externalTask, variablesMap);



                })
                .open();
    }
}