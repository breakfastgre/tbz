package testers;

import org.camunda.bpm.client.ExternalTaskClient;

import java.awt.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class RecV {
    private final static Logger LOGGER = Logger.getLogger(RetV.class.getName());

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("test-thing2")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {

                    String myVariable = externalTask.getVariable("myVariable");

                    LOGGER.info("Variable received: " + myVariable);

                    System.out.println("##########################");
                    System.out.println(myVariable); //Why is this always null?
                    System.out.println("##########################");

                    externalTaskService.complete(externalTask);



                })
                .open();
    }
}