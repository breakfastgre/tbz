package newReminder.camunda.bpmn.newReminder;

import javax.swing.*;
import java.awt.FlowLayout;

public class Reminder {
    public JFrame reminderWindow;
    public JLabel reminderLabel;
    public ReminderButton ok = new ReminderButton("ok", this);
    public ReminderButton rem1h = new ReminderButton("1h", this);
    public ReminderButton rem1d = new ReminderButton("1d", this);
    public String reminderText;

    public boolean snooz1h = false;
    public boolean snooze1d = false;
    public boolean closed = false;

    public Reminder(String reminderText) {
        this.reminderText = reminderText;
        reminderWindow = new JFrame(this.reminderText);
        reminderWindow.setLayout(new FlowLayout()); // Explicitly set the layout

        reminderLabel = new JLabel(this.reminderText , JLabel.CENTER);
        reminderWindow.add(reminderLabel);


        reminderWindow.add(ok);
        reminderWindow.add(rem1h);
        reminderWindow.add(rem1d);

        reminderWindow.setSize(300, 200);
        reminderWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Ensure the application exits when the window is closed
    }

    public void showReminder() {
        reminderWindow.setVisible(true);
    }

    public void snooze1h(){
        snooz1h = true;
    }

    public void snooze1d(){
        snooze1d = true;
    }

    public void closeWindow(){
        Timer timer = new Timer(500, e -> reminderWindow.dispose());
        timer.start();
        closed = true;
    }

}
