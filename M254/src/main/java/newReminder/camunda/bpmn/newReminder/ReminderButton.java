package newReminder.camunda.bpmn.newReminder;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;


public class ReminderButton extends JButton {
    public boolean pressed;
    public String kind;
    Color bColor;
    public Reminder myParentReminder;

    public ReminderButton(String kind, Reminder r){
        super();
        this.kind = kind;
        this.myParentReminder = r;
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pressed = true;
                changeButton();
                switch(kind){
                    case "1h":
                        myParentReminder.snooze1h();
                    break;
                    case "1d":
                        myParentReminder.snooze1d();
                    break;
                    case "ok":
                    break;
                    default:
                        System.out.println("Something has gone horribly wrong");
                }
                myParentReminder.closeWindow();
            }
        });

        switch(kind){
            case "ok":
                this.setButtonText("Ok");
                bColor = new Color(Integer.parseInt("9ec866", 16));
            break;
            case "1h":
                this.setButtonText("Snooze 1 Hour");
                bColor = new Color(Integer.parseInt("fbe13d", 16));
            break;
            case "1d":
                this.setButtonText("Snooze 1 Day");
                bColor = new Color(Integer.parseInt("da4545", 16));
            break;
            default:
                this.setButtonText("Something has gone wrong");
        }

        this.setBackground(bColor);

    }

    public void setButtonText(String text){
        this.setText(text);

    }

    public void changeButton(){
        this.setBackground(Color.BLUE);
        this.setOpaque(true);
    }

}
