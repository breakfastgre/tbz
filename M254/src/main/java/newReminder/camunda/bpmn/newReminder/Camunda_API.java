package newReminder.camunda.bpmn.newReminder;

import org.camunda.bpm.client.ExternalTaskClient;

import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Map;

public class Camunda_API {
    private final static Logger LOGGER = Logger.getLogger(Camunda_API.class.getName());

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(10000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process püsh it to the limit
        client.subscribe("show-reminder")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {

                    // Get a process variable
                    String item = externalTask.getVariable("locRemText");

                    Map<String, Object> variablesMap = new HashMap<>();

                    Reminder r1 = new Reminder(item);
                    r1.showReminder();

                    while(!r1.closed){
                        System.out.println("waiting");
                        try {
                            // Pause execution for 5 seconds
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            // Handle the exception if the thread is interrupted
                            Thread.currentThread().interrupt(); // good practice to re-interrupt the thread
                            System.err.println("Thread was interrupted: " + e.getMessage());
                        }

                    }

                    String snooze1h = "no";
                    String snooze1d = "no";

                    if(r1.snooz1h){
                        snooze1h = "yes";
                        System.out.println("skdfsdlkfjhsdlkfj");
                    } else if (r1.snooze1d){
                        snooze1d = "yes";
                    }

                    variablesMap.put("snooze1h", snooze1h);
                    variablesMap.put("snooze1d", snooze1d);

                    externalTaskService.complete(externalTask, variablesMap);
                })
                .open();
    }
}