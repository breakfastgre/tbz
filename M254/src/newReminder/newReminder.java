package newReminder;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;

public class newReminder {
    public static void main(String[] args) {
        // Create a new JFrame
        JFrame frame = new JFrame("New org.camunda.bpmn.newReminder.Reminder");
        frame.setSize(600, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        JLabel labelMessage = new JLabel("Enter your reminder message:");
        JLabel lableDate = new JLabel("Enter Date in Format: hh:mm-dd-mm-yyyy");
        JButton button = new JButton("Submit");
        JTextField textFieldMessage = new JTextField(20);
        JTextField textFieldDate = new JTextField(20);

        button.addActionListener(e -> {
            String reminderText = textFieldMessage.getText();
            String reminderDate = textFieldDate.getText();
            reminderText = ""+ reminderText +"\n"+reminderDate;
            try {
                writeToFile(reminderText);
                frame.dispose();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        panel.add(labelMessage);
        panel.add(textFieldMessage);
        panel.add(lableDate);
        panel.add(textFieldDate);
        panel.add(button);

        frame.add(panel);
        frame.setVisible(true);
    }
    public static void writeToFile(String reminderText) throws IOException {
        FileWriter MyWriter = new FileWriter("org.camunda.bpmn.newReminder.Reminder/src/ReminderFile/org.camunda.bpmn.newReminder.Reminder");
        MyWriter.write(reminderText);
        MyWriter.close();
    }
}
